export class UserDeleteRequest {
    userIds : string[];
    adminId : string;
    accountStatus :string;
}

export class UserEditRequest {
    adminId : string;
    userData : {
        id : string;
        firstname : string;
        lastname : string;    
    }
}
