import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';
import {SidePanelComponent} from './side-panel/side-panel.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { Routes, RouterModule } from '@angular/router';
import { PageUnderMaintenanceComponent } from './page-under-maintenance/page-under-maintenance.component';
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { AgGridModule } from 'ag-grid-angular';
import { CellCustomComponent } from './cell-custom/cell-custom.component';
//import {PopupModule} from 'ng2-opd-popup';



const appRoutes : Routes = [
  {path:'',component:DashboardComponent},
  {path:'login',component:LoginComponent},
  {path:'not-found',component:PageUnderMaintenanceComponent},
  {path:'users',component:UsersComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidePanelComponent,
    DashboardComponent,
    LoginComponent,
    MainPageComponent,
    PageUnderMaintenanceComponent,
    UsersComponent,
    CellCustomComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    AgGridModule.withComponents([]),  
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

