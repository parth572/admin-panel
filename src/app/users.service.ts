import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { LoginData } from './login-data';
import { MessageService } from './message.service';
import { UserDeleteRequest, UserEditRequest } from './user-delete-request';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' },)
};

@Injectable({ providedIn: 'root' })
export class UsersDataService {
  UserId = "5baa1e31bf504f149794066e";
  access_token1 = "fFktbYMDIMUeV8qLPZPlW59kBGNh2L8J2BW1P6cU8glQu2oXVYr43pKgl7ewRVk2";
 // access_token="qv9b8BjWLzk0HzSGtZiEQ5rJNNMJQMaUtjLK9EpIQvwp2zujsEJY2MiJMO5ArUV9";
  private getUsersUrl = '//13.232.160.166:3001/api/users/getAllUsers';  // URL to web api
  private suspendedUsersUrl = '//13.232.160.166:3001/api/users/suspendedUsers';  // URL to web api
  private deleteUsersUrl = '//13.232.160.166:3001/api/users/deleteUsers';  // URL to web api
  private editUsersUrl = '//13.232.160.166:3001/api/users/updateUser'; 

  constructor(private http: HttpClient,private messageService: MessageService) { }

  //////// Save methods //////////
  /** POST: add a new hero to the server */
  getAllUsers (): Observable<any> {
    return this.http.get(`${this.getUsersUrl}/?userId=${this.UserId}&access_token=${this.access_token1}`).pipe(      
      tap(() => this.log(`getAllUsers w/ id`)),
      catchError(this.handleError('getAllUsers'))
    );
  }

  suspendUsers (suspendUsers: UserDeleteRequest): Observable<any> {
    console.log(suspendUsers);    
      return this.http.patch<UserDeleteRequest>(`${this.suspendedUsersUrl}/?access_token=${this.access_token1}`,suspendUsers).pipe(
      tap((hero: any) => this.log(`suspendUsers w/ id`)),
      catchError(this.handleError<UserDeleteRequest>('suspendUsers'))
    );
  }

  deleteUsers (deleteUsers: UserDeleteRequest): Observable<UserDeleteRequest> {
    return this.http.post<UserDeleteRequest>(this.deleteUsersUrl,deleteUsers).pipe(
      tap((hero: UserDeleteRequest) => this.log(`deleteUsers w/ id`)),
      catchError(this.handleError<UserDeleteRequest>('deleteUsers'))
    );
  }

  editUsers (editUsers: UserEditRequest): Observable<UserEditRequest> {
    console.log(editUsers);
    return this.http.patch<UserEditRequest>(`${this.editUsersUrl}/?access_token=${this.access_token1}`,editUsers).pipe(
      tap((hero: UserEditRequest) => this.log(`editUsers w/ id`)),
      catchError(this.handleError<UserEditRequest>('editUsers'))
    );
  }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a UsersService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`UsersService: ${message}`);
  }
}