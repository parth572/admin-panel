import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { LoginData } from './login-data';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class HeroService {

  private heroesUrl = '//13.232.160.166:3001/api/users/login';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }


  //////// Save methods //////////

  /** POST: add a new hero to the server */
  logInUser (hero: LoginData): Observable<LoginData> {
    return this.http.post<LoginData>(this.heroesUrl, hero).pipe(
      tap((hero: LoginData) => this.log(`logInUser w/ id`)),
      catchError(this.handleError<LoginData>('logInUser'))
    );
  }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }
}