import { Component, OnInit,ViewChild } from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {Router} from "@angular/router";
import { UsersDataService } from '../users.service';
import { UserDeleteRequest, UserEditRequest } from '../user-delete-request';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
//import {Popup} from 'ng2-opd-popup';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent  {
   // @ViewChild('popup1') popup1: Popup;
    allowEdit = false;
    allowSuspend = false;
    isAdminDeleted = false;
    private gridApi;
    private gridColumnApi;
    private rowSelection;
    GridOptions : any;
    private autoGroupColumnDef;
    rowData = [];

    requestMessage: UserDeleteRequest = {
      adminId : "5baa1e31bf504f149794066e",
      userIds : [],
      accountStatus : "suspend"
    };

    editRequestMessage: UserEditRequest = {
        adminId : "5baa1e31bf504f149794066e",
        userData : {
          id : "",
          firstname : "",
          lastname : ""   
      }
    };

    columnDefs = [{headerName: "Select",checkboxSelection: true, width :100},
          {headerName: 'FirstName', field: 'firstname',editable:true, width :180},
          {headerName: 'LastName', field: 'lastname' ,editable:true,width :180},
          {headerName: 'Email', field: 'email',editable:true,width :200},
          {headerName: 'PhoneNumber', field: 'phone_number',editable:true,width :180},
          {headerName: 'UserName', field: 'username' ,editable:true ,width :180}, 
    ];
   
    onGridReady(params) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    }

    constructor(private usersDataService: UsersDataService, private router : Router) {
      this.rowSelection = "multiple";
      this.getGridData();
     
    }
    
    getGridData() {
      this.usersDataService.getAllUsers().subscribe(userData => {
        if (!userData || !userData.success){alert("Somthing went wrong");}
        else{
            this.rowData= userData.data;
        }
    });
    }

    onSelectionChanged() {
      var selectedRows = this.gridApi.getSelectedRows();
      this.allowEdit =  selectedRows.length == 1 ? true : false;
      this.allowSuspend = selectedRows.length > 0 ? true : false;
    }

    isAdmin(selectedData) : boolean{
      var isAdmin = false;
      var adminAvailable = selectedData.map(function (obj) {
        return obj.isAdmin;
      });
      return isAdmin = adminAvailable.indexOf("true") !== -1 ? true : false ;
    }

    getListOfIds(selectedData) : any{
      var id = [];
      var getListOfIds = selectedData.map(function (obj) {
        return obj.id;
      });
    }

    onEditSelected() {
      var selectedData = this.gridApi.getSelectedRows();
      this.setEditRequest(selectedData);
      this.usersDataService.editUsers(this.editRequestMessage).
      subscribe(editUserResponse => {
        if(!!editUserResponse){
          alert("User updated Succesfully");
          this.getGridData();
        }
      });        
    }

    onSuspendSelected() {
      var selectedData = this.gridApi.getSelectedRows();
      var getListOfIds = selectedData.map(function (obj) {
        return obj.id;
      });
      var isAdmin = this.isAdmin(selectedData);
      if(isAdmin){alert("Admin user cannot be deleted");}
      else{
        this.requestMessage.userIds = getListOfIds;
        this.usersDataService.suspendUsers(this.requestMessage).
        subscribe(suspendUserResponse => {
          if(!!suspendUserResponse && suspendUserResponse.success){
            var res = this.gridApi.updateRowData({ remove: selectedData });
            alert("Suspended Succesfully");
          }
        });     
      }
    }

    setEditRequest(selectedData)  {
        var selectedDataObject = selectedData[0]
        this.editRequestMessage.adminId = this.editRequestMessage.adminId;
        this.editRequestMessage.userData.firstname = selectedDataObject.firstname;
        this.editRequestMessage.userData.lastname = selectedDataObject.lastname;
        this.editRequestMessage.userData.id = selectedDataObject.id;
    }

    onDeleteSelected() {
        var selectedData = this.gridApi.getSelectedRows();
        var res = this.gridApi.updateRowData({ remove: selectedData });
        var getListOfIds = selectedData.map(function (obj) {
          return obj.id;
        });

        var isAdmin = this.isAdmin(selectedData);
        if(isAdmin){alert("Admin user cannot be deleted");}
        else{
          this.requestMessage.userIds = getListOfIds;
          this.usersDataService.deleteUsers(this.requestMessage).
          subscribe(deleteUserResponse => {});   
        }
    }
}