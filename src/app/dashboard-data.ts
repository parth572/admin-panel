export class Dashboard {
    color: string;
    backgroundColor: string;
    value : string;
    text : string;
    icon : string;
}

export class ChartMap {
    columnName: string;
    chartMapName: string;
    backgroundColor : string;
    columnId : string;
}

export const DashboardData: Dashboard[] = [
{ 
    color: "#4F84DA",
    backgroundColor:  "#E8EDF6",
    value : "1000",
    text : "Total Users",
    icon : "assets/images/man-user.png"
},
{ 
    color: "#89DC5A",
    backgroundColor:  "#EDF6E8",
    value : "1000",
    text : "Total Groups",
    icon : "assets/images/users_green.png"
},
 { 
    color: "#FF8617",
    backgroundColor:  "#FBEDDF",
    value : "1000 TB",
    text : "Total User Storage",
    icon : "assets/images/servers.png"
},
 { 
    color: "#FB0437",
    backgroundColor:  "#FCDFDF",
    value : "3000",
    text : "Active Users",
    icon : "assets/images/users_red.png"
},
 { 
    color: "#40D4DB",
    backgroundColor: "#E7F5F6",
    value : "200",
    text : "Total Countries",
    icon : "assets/images/countries.png"
}];

export const ChartMapData: ChartMap[] = [
{ 
    columnName: "col-lg-7",
    chartMapName:  "User Location distribution",
    backgroundColor : "#E8EDF6",
    columnId : "#mapdiv"
},
{ 
    columnName: "col-lg-5",
    chartMapName:  "User storage plan distribution",
    backgroundColor : "#05436B",
    columnId : "#chartdiv"
}];