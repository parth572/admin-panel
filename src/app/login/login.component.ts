import { Component, OnInit } from '@angular/core';
import { LoginData } from '../login-data';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HeroService } from '../login.service';
import { string } from '@amcharts/amcharts4/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: LoginData = {
    password : "",
    email : "cr.tagadiya@gmail.com"
  };
  
  constructor(private heroService: HeroService, private router : Router) { }

  ngOnInit() {
  }

  loginUser(title:string, pass:string) {
    this.credentials.password = pass;
    this.credentials.email = title;
    this.heroService.logInUser(this.credentials)
      .subscribe(hero => {
        if (!hero){alert("Please enter valid credential");}
        else{this.router.navigate(['']);}
      });
  }
}