import {Component,NgZone,OnInit} from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";

import { DashboardData,ChartMapData } from '../dashboard-data';

// Importing themes
am4core.useTheme(am4themes_animated);

declare var AmCharts :any;
@Component({
    selector:'app-root-dashboard',
    templateUrl:'./dashboard.component.html'
})

export class DashboardComponent implements OnInit{

    dashboardData = DashboardData;
    chartMapData = ChartMapData
    private chart: am4charts.XYChart;

    ngOnInit() {
    }

    ngAfterViewInit() {
        // Create chart
        let chart = am4core.create("chartdiv", am4charts.XYChart);
            // Add Data
            chart.data = [{
                "country": "2 GB",
                "visits": 180,
                "color": "#045A8B"
            }, {
                "country": "10 GB",
                "visits": 230,
                "color": "#505C4D"
            },{
                "country": "100 GB",
                "visits": 415,
                "config": {
                  "color": "#FB0437"
                },
            }, {
                "country": "2 TB",
                "visits": 70,
                "color": "#00708C"
            }]

        // Add category axis
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.renderer.minGridDistance = 1;
        // Add value axis
        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Add series
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.name = "Web Traffic";
        series.dataFields.categoryX = "country";
        series.dataFields.valueY = "visits";
        series.heatRules.push({
          "target": series.columns.template,
          "property": "fill",
          "min": am4core.color("#00708C"),
          "max": am4core.color("#277266"),
          "dataField": "valueY"
        });

// Maps
let map = am4core.create("mapdiv", am4maps.MapChart);
map.geodata = am4geodata_worldLow;
map.projection = new am4maps.projections.Miller();
map.zoomControl =  new am4maps.ZoomControl();
map.zoomControl.align = "left";
map.zoomControl.paddingBottom = 280;
map.zoomControl.paddingLeft = 10;


// Create map polygon series
var polygonSeries = map.series.push(new am4maps.MapPolygonSeries());
// Make map load polygon (like country names) data from GeoJSON
polygonSeries.useGeodata = true;
// Configure series
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}";
polygonTemplate.fill = am4core.color("#0D5685");

// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = am4core.color("#0D5685");

// Remove Antarctica
polygonSeries.exclude = ["AQ"];
// Bind "fill" property to "fill" key in data
polygonTemplate.propertyFields.fill = "fill";

// Create image series
var imageSeries = map.series.push(new am4maps.MapImageSeries());

// Create a circle image in image series template so it gets replicated to all new images
var imageSeriesTemplate = imageSeries.mapImages.template;

var circle = imageSeriesTemplate.createChild(am4core.Circle);
circle.radius = 4;
circle.fill = am4core.color("#91DB59");
circle.stroke = am4core.color("#FFFFFF");
circle.strokeWidth = 1;
circle.nonScaling = true;
circle.tooltipText = "{title}";

// Set property fields
imageSeriesTemplate.propertyFields.latitude = "latitude";
imageSeriesTemplate.propertyFields.longitude = "longitude";

// Add data for the three cities
imageSeries.data = [{
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Vienna",
    "latitude": 48.2092,
    "longitude": 16.3728
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Minsk",
    "latitude": 53.9678,
    "longitude": 27.5766
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Brussels",
    "latitude": 50.8371,
    "longitude": 4.3676
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Sarajevo",
    "latitude": 43.8608,
    "longitude": 18.4214
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Sofia",
    "latitude": 42.7105,
    "longitude": 23.3238
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Zagreb",
    "latitude": 45.8150,
    "longitude": 15.9785
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Pristina",
    "latitude": 42.666667,
    "longitude": 21.166667
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Prague",
    "latitude": 50.0878,
    "longitude": 14.4205
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Copenhagen",
    "latitude": 55.6763,
    "longitude": 12.5681
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Tallinn",
    "latitude": 59.4389,
    "longitude": 24.7545
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Helsinki",
    "latitude": 60.1699,
    "longitude": 24.9384
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Paris",
    "latitude": 48.8567,
    "longitude": 2.3510
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Berlin",
    "latitude": 52.5235,
    "longitude": 13.4115
  },
  {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Caracas",
    "latitude": 10.4961,
    "longitude": -66.8983
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Oranjestad",
    "latitude": 12.5246,
    "longitude": -70.0265
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Cayenne",
    "latitude": 4.9346,
    "longitude": -52.3303
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Plymouth",
    "latitude": 16.6802,
    "longitude": -62.2014
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "San Juan",
    "latitude": 18.4500,
    "longitude": -66.0667
  }, {
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Algiers",
    "latitude": 36.7755,
    "longitude": 3.0597
  }];  
}}